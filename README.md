# API-tools contains helpful API tooling for F5XC 

## SDK Generation

The "generateSDK.py" script is used to help create SDK clients for the desired swagger files. F5XC produces a seperate swagger file for each API path. Because of this, there are 100+ different swagger files that could be built. Also, combining the SDK clients into a rollup SDK is not currently possible. This means a SDK client is required for each API path desired. An example is the "cert-expire.py" helper, which uses the Namespace and Load Balancer SDK clients. 


### Update Swagger Files:

Change into the directory "codegen/in/swagger"

Then download any swagger file you wish to use. To find these swagger files, navigate to the Dev Portal found at https://<tenant>.console.ves.volterra.io/web/devportal/domain. 

Enter your F5XC domain and click "Access Portal". 

On the left hand side of the page, select the API section you wish to use. Then on the top of the page find the swagger.json file  and download the file. Make sure these files are stored under "codegen/in/swagger".

The "generateSDK.py" script will look for any ".json" file under "codegen/in/swagger", so the nested directory path is not an issue.

The generate code uses a regex to create a shorter name to make clients easier to work with. The regex is below for reference. If your swagger file does not match this pattern, a short name will not be used and the entire file name will be used for the client import name.

```
pattern = re.compile(r'.*schema\.([a-z_\.0-9]+)\.ves-swagger\.json')
```

## Generate SDK

The generateSDK.py script wraps around Swagger.io's codegen Docker image, located here: https://hub.docker.com/u/swaggerapi.

You can build your own local docker container directly from Swagger.io's source code located here: https://github.com/swagger-api/swagger-codegen#docker. If you chose to do that, make sure to change "swaggerapi/swagger-codegen-cli" to your local docker tag name in the file "generateSDK.py". 

To list all Swagger files found by the script, run this command.

```
python3 generateSDK.py --listAllSwagger
...
<truncated output>
swagger: alert file_path: codegen/in/swagger/docs-cloud-f5-com.0010.public.ves.io.schema.alert.ves-swagger.json
swagger: container_registry file_path: codegen/in/swagger/docs-cloud-f5-com.0044.public.ves.io.schema.container_registry.ves-swagger.json
swagger: app_firewall file_path: codegen/in/swagger/docs-cloud-f5-com.0015.public.ves.io.schema.app_firewall.ves-swagger.json
swagger: shape_client_side_defense file_path: codegen/in/swagger/docs-cloud-f5-com.0027.public.ves.io.schema.shape.client_side_defense.ves-swagger.json
```

To list all SDK's already generated, run the command below.

```
python3 generateSDK.py --listAllSDK

python-alert
python-lb
python-ns
```

There are two methods to create SDK clients. The first method is creating one client SDK at a time. 
The second method uses a helper to generate multiple client SDK's, and writes version info. 
The second method is perfered.

### Method One: Manual SDK client generation
To generate a SDK client, get the short name from the "listAllSwagger" command, and provide that to the "--generate" flag.

```
python3 generateSDK.py --generate shape_client_side_defense
```

To confirm the new SDK was created, use the "--listAllSDK" option to see if the new SDK is listed.

```
python3 generateSDK.py --listAllSDK

python-alert
shape_client_side_defense
python-lb
python-ns
```

### Method Two: Docker Prep SDK multiple client generation
This method is used to maintain the required SDK's to support any helper scripts. This process is driven from a config file. The 'sdk.requirements' file contains a list of the required SDK's that must be generated. Use the 'generateSDK.py --listAllSwagger' to see the options, and use the short name listed first, e.g. 'namespace'. 

To generate the needed SDK's, use the following command once the 'sdk.requirements' config is updated.

```
python3 dockerSDKPackager.py
```

On success, you should see the SDK's under the folder 'codegen/out/{name_of_swagger_client}'. 
You can also review the 'sdk.version' file, which shows a version, the author, and the list of SDK's included. 

There will be a sample helper script created for the last SDK in the 'sdk.requirements' file under the 'helpers/sample' folder. The name of the sample script will match the client SDK name, e.g., 'fx5x_namespace_client-sample.py' for the 'namespace' swagger file generated from.

Now, create the docker image. You can use the local docker image for running the helpers, or tag the docker image and upload to a cloud provider.

### Create Docker Image

Any new helper script, or required SDK client for a helper, will require a new docker container be created. The Docker build copies and installs all Python SDK's found in the "codegen/out" folder, so make sure to run the "generateSDK.py" script first. 

```
docker build -t api-tools .
```

### Run Docker Container

Steps to run with docker. This maps local dir "myConfig" to the docker folder "/config". This is for passing config files. You can use whatever local directory you wish, just update the volume mapping below. 

```
docker run --rm -v ${PWD}/myConfigs/:/configs api-tools
```

If no option is given to the docker container, then the "showAll.sh" script will run that just prints all the helper files available. 

```
docker run --rm  api-tools /work/certs/cert-expire.py
```

To run a specific helper, call it and any needed options.

```
docker run --rm  -v ${PWD}/myConfigs:/configs api-tools python3 certs/cert-expire.py --config /configs/config.json
```

If you wish to run multiple helper scripts inside the docker container, run in interactive mode.

```
docker run --rm -it -v ${PWD}/myConfigs:/configs api-tools /bin/bash

root@a786482c60d8:/helpers# ls
certs  showAll.sh

root@a786482c60d8:/helpers# python3 certs/cert-expire.py  
```


## Helpers

Many of the Helpers use generated SDK code. Due to this, it is easier to have these helpers run inside a docker container with the correct Python enviornemnt created. 

### Config 
Helpers that are generated contain a 'config' method for taking the API Host and APIToken values.

Create a config JSON file containing the "Host" and "APIToken". The "Host" is the api endpoint, ex: "https://nirvana.console.ves.volterra.io", where "nirvana" represents the Tenant. The "APIToken" is the API Token created for the given API endpoint. Keep this file private, and do NOT include it to this repo. 

Here is an example:

```
{
    "Host" : "https://<tenant>.console.ves.volterra.io",
    "APIToken" : "<REMOVED>"
}
```
### Helper Script Details
To see more details about each helper script, view the file 'helpers/README.md'.

