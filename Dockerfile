FROM alpine:latest

RUN apk update && apk upgrade && apk add --no-cache python3 py3-pip curl
COPY helpers /helpers/
COPY codegen/out /sdk/

RUN python3 -m pip install /sdk/* && chmod a+x /helpers/showAll.sh

WORKDIR /helpers

CMD ["/helpers/showAll.sh"]
