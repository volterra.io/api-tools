import sys
import os
import getpass
from string import Template
from generateSDK import Swag

def getSDKList():
    try:
        f = open('sdk.requirements', 'r')
        data = f.read()
        f.close()
    except Exception as e:
        print("ERROR with SDK requirements open, ", e)
        sys.exit(2)
    return data

def getLastVer():
    try:
        f = open('sdk.version', 'r')
        data = f.read()
        f.close()
    except Exception as e:
        print("ERROR with SDK version open, ", e)
        sys.exit(2)
    myList = data.split('\n')
    reverseList = myList[::-1]  

    parts = []
    for l in reverseList:
        if 'Version:' in l:
            parts = l.split(':')
            break
    if len(parts) != 2:
        print("ERROR getting version", parts)
        sys.exit(2)
    lastVer = ''.join(parts[-1].strip())
    return int(lastVer)
    
def writeVersion(sdks):
    un = getpass.getuser()
    lastVer = getLastVer()
    myVer = lastVer + 1

    out = '''
#############################

Version: {ver}
Author: {author}
SDK list:
{sdkList}
#############################'''.format(ver = myVer, author = un, sdkList = sdks)
    try:
        f = open('sdk.version', 'a')
        f.write(out)
        f.close()
    except Exception as e:
        print("ERROR writing new version file, ", e)
        sys.exit(2)

def generateSDK(sdks):
    mySwag = Swag()
    mySwag.getSwaggerFiles()

    for l in sdks:
        if len(l) > 1:
            mySwag.generateSDK(l)

def generateSample(sdks):
    slist = sdks.split('\n')
    slist = [i for i in slist if i]
    sdk = slist[-1]

    helper = ''
    fullClientName = 'f5xc_{}_client'.format(sdk)
    try:
        f = open('helpers/sample/sample.py')
        data = f.read()
        f.close()
        sdkTemplate = Template(data)
        helper = sdkTemplate.substitute(client=fullClientName)
    except Exception as e:
        return "ERROR with generateSample for sdk: {} with error: {}".format(fullClientName, e)

    try:
        f = open('helpers/sample/{}-sample.py'.format(fullClientName), 'w')
        f.write(helper)
        f.close()
    except Exception as e:
        return "ERROR with generateSample writing sample file for skd: with error: {}".format(fullClientName, e)
    return ''

sdks = getSDKList()
print(sdks)
err = generateSDK(sdks.split('\n'))
if err is not None:
    print(err)
    sys.exit(2)
writeVersion(sdks)
mySwag = Swag()
mySwag.listAllSDK()
generateSample(sdks)