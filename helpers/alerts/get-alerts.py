import f5xc_alert_client
import argparse
import sys
import json
import pprint
import ast

sys.path.append('../')

import helpers.common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)

class AlertResults(object):
    def __init__(self, rawData=''):
        self.rawData = rawData
        self.fmtData = ''
        self.myAlerts = {} 
        self.process()

    def process(self):
        myAlerts = {}
        alertsRaw = self.rawData
        myJson = ast.literal_eval(alertsRaw)
        data = json.loads(json.dumps(myJson))
        fmtData = json.loads(data['data'])

        for alert in fmtData:
            if 'labels' in alert:
                if 'alertname' in alert['labels']:
                    name = alert['labels']['alertname']
                    if name not in myAlerts.keys():
                        myAlerts[name] = []
                    myAlerts[name].append(alert['labels'])

        self.myAlerts = myAlerts
        return myAlerts

    def getAlertTypes(self):
        return list(self.myAlerts.keys())

    def getAlertsNamed(self, alertName):
        if alertName not in self.myAlerts:
            print("ERROR: Given alertname: {} not found in Alerts with keys: {}".format(alertName, self.myAlerts.keys()))
            return ''
        return self.myAlerts[alertName]

parser = argparse.ArgumentParser(description="f5xc_alert_client Helper")
parser.add_argument('--config', default='config.json', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
parser.add_argument('--listAlertTypes', action='store_true', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
parser.add_argument('--listAllAlerts', action='store_true', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
parser.add_argument('--listNamedAlerts', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')

args = parser.parse_args()

myConfig = config.Config(f5xc_alert_client)
myConfig.readConfig(args.config)

myClient = f5xc_alert_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
apiInstance = f5xc_alert_client.DefaultApi(myClient)

allAlerts = apiInstance.ves_io_schema_alert_custom_api_alerts('system')
myAlerts = AlertResults(allAlerts.to_str())

if bool(args.listAlertTypes):
    for at in myAlerts.getAlertTypes():
        print('{}'.format(at))
    sys.exit(0)

if bool(args.listAllAlerts):    
    pprint.pprint(myAlerts.myAlerts)
    sys.exit(0)

if args.listNamedAlerts is not None:
    print(myAlerts.getAlertsNamed(args.listNamedAlerts))
