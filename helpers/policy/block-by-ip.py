import f5xc_service_policy_client
import f5xc_ip_prefix_set_client
import argparse
import sys
import pprint

sys.path.append('../')

import helpers.common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)


def createPolicy(apiInstance , namespace, policyName, ipPrefixSet):
    body =  {"metadata": {
            "description": policyName,
            "name": policyName,
            "namespace": namespace },
            "spec": {"deny_list": {"ip_prefix_set": [
            {"name": ipPrefixSet,
            "namespace": namespace,
             }]}}}
    try:
        apiInstance.ves_io_schema_service_policy_api_create(namespace, body)
    except Exception as e:
        print('createPolicy error', e)
        sys.exit(2)

def createPrefixSet(apiInstance, namespace, ipPrefixSet, ip):
    body =  {"metadata": {
        "name": ipPrefixSet,
        "namespace": namespace },
    "spec": { "prefix": ['{}/32'.format(ip)] } }
    try:
        apiInstance.ves_io_schema_ip_prefix_set_api_create(namespace, body)
    except Exception as e:
        print('createPrefixSet error', e)
        sys.exit(2)

def updatePrefixSet(apiInstance, namespace, ipPrefixSet, prefix):
    body =  {"metadata": {
        "name": ipPrefixSet,
        "namespace": namespace },
    "spec": { "prefix": prefix } }
    try:
        apiInstance.ves_io_schema_ip_prefix_set_api_replace(namespace, ipPrefixSet, body)
    except Exception as e:
        print('updatePrefixSet error: ', e)    
        sys.exit(2)

parser = argparse.ArgumentParser(description="f5xc_service_policy_client Helper")
parser.add_argument('--config', default='config.json', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
parser.add_argument('--namespace', help='namespace where load balancer resides')
parser.add_argument('--policyName', default='block-malicious-users', help='service policy name assigned to load balancer')
parser.add_argument('--ipPrefixSet', default='block-malicious-users', help='ip prefix set that contains IP addresses to block and is attached to service policy')
parser.add_argument('--blockIP', help='IP to block')
parser.add_argument('--removeIP', help='remove blocked IP')
parser.add_argument('--viewBlockIP', action='store_true', help='view the currently blocked IP addresses')

args = parser.parse_args()

if args.blockIP is None and args.removeIP is None and args.viewBlockIP is None:
    print("Block or Remove IP is required")
    sys.exit(2)

# create Service Policy Client and API Instance
myConfig = config.Config(f5xc_service_policy_client)
myConfig.readConfig(args.config)

myClient = f5xc_service_policy_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
apiInstance = f5xc_service_policy_client.DefaultApi(myClient)

try:
    # Check that service policy exists
    spec=apiInstance.ves_io_schema_service_policy_api_get(args.namespace, args.policyName).spec

except Exception as e:
    if '{} does not exist'.format(args.policyName) in str(e):
        print('Policy {} not found, creating'.format(args.policyName))
        createPolicy(apiInstance, args.namespace, args.policyName, args.ipPrefixSet)
    else:
        print('policy check error', e)
        sys.exit(2)

# create IP Prefix Set Client and API Instance
myIPConfig = config.Config(f5xc_ip_prefix_set_client)
myIPConfig.readConfig(args.config)
myIPPClient = f5xc_ip_prefix_set_client.ApiClient(myIPConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myIPConfig.apiToken))
apiInstanceIP = f5xc_ip_prefix_set_client.DefaultApi(myIPPClient)

try:
    # Check that ip prefex exists
    response = apiInstanceIP.ves_io_schema_ip_prefix_set_api_get(args.namespace, args.ipPrefixSet)
    currentPrefix = response.spec.prefix

    if bool(args.viewBlockIP):
        print(currentPrefix)
        sys.exit(0)

    if args.blockIP is not None:
        currentPrefix.append('{}/32'.format(args.blockIP))

    if args.removeIP is not None:
        formatted = '{}/32'.format(args.removeIP)
        if formatted not in currentPrefix:
            print("Remove IP: {} is not present in IP Prefix Set: {}".format(args.removeIP, args.ipPrefixSet))
            sys.exit(0)
        currentPrefix.remove(formatted)

    updatePrefixSet(apiInstanceIP, args.namespace, args.ipPrefixSet, currentPrefix)

except Exception as e:
    if '{} does not exist'.format(args.ipPrefixSet) in str(e):
        print('Policy {} not found, creating'.format(args.ipPrefixSet))
        createPrefixSet(apiInstanceIP, args.namespace, args.ipPrefixSet, args.blockIP)
    else:
        print(e)
        sys.exit(2)

