# Helpers

Many of the Helpers use generated SDK code. Due to this, it is easier to have these helpers run inside a docker container with the correct Python enviornemnt created. 

## Config 
Helpers that are generated contain a 'config' method for taking the API Host and APIToken values.

Create a config JSON file containing the "Host" and "APIToken". The "Host" is the api endpoint, ex: "https://nirvana.console.ves.volterra.io", where "nirvana" represents the tenant. The "APIToken" is the API Token created for the given API endpoint. Keep this file private, and do NOT include it to this repo. 

Here is an example:

```
{
    "Host" : "https://<tenant>.console.ves.volterra.io",
    "APIToken" : "<REMOVED>"
}
```

## Running Helpers
There are two methods to running the helpers. Method one uses the Docker image. Method two uses a local virtual python3 environment. 

### Docker Method
You can run the helpers without setting up a local enviornment using docker. The volume mapping is so the required config file will be present on the docker container. You will need to create this config file, see the above 'Config' section for details. 

```
docker run --rm  -v ${PWD}/myConfigs:/configs api-tools python3 <helper_dir>/<helper_name> --config /configs/config.json
```

If you want to run multiple helpers, start the docker container to run a shell. The inside the docker container you can run multiple commands, one after another.

```
docker run --rm  -it -v ${PWD}/myConfigs:/configs api-tools /bin/sh
```

### Local Method
Setup virtual environment.  'python3 -m venv venv'
Activate the environmnet. 'source venv/bin/activate'
Install all SDKS. 'python3 -m pip install codegen/out/*'

Now you can run the helper script locally with 'python3'. 

## certs/cert-expire.py
This script will walk all Namespaces under the provided tenant and list all Load Balancers using HTTPS and when the certificate expires. 

```
python3 certs/cert-expire.py --config /configs/config.json
```

## api-creds/api-creds.py
This script will show all API credentials and help delete them. A config is required, see above section for example.

List all Credentials
```
python3 api-creds/api-creds.py --config /configs/config.json --listAllCreds
...... truncated ......
 'nginx-200-iewliwlr': {'active': True,
 'create_timestamp': datetime.datetime(2021, 11, 11, 21, 26, 57, 647325, tzinfo=tzutc()),
 'expiry_timestamp': datetime.datetime(2021, 12, 31, 21, 26, 57, 647333, tzinfo=tzutc()),
 'name': 'nginx-200-iewliwlr',
 'namespace': 'system',
 'type': 'KUBE_CONFIG',
 'uid': '536abc7d-f033-4669-a745-6b09108d5eca',
 ...... truncated ......
 ```


List all Credential Types
```
python3 api-creds/api-creds.py --config /configs/config.json --listTypes

dict_keys(['KUBE_CONFIG', 'API_TOKEN', 'API_CERTIFICATE', 'SITE_GLOBAL_KUBE_CONFIG'])
```

List all Credentials by Type
```
python3 api-creds/api-creds.py --config /configs/config.json --listAllType 'KUBE_CONFIG

...... truncated ......
 'nginx-200-iewliwlr': {'active': True,
 'create_timestamp': datetime.datetime(2021, 11, 11, 21, 26, 57, 647325, tzinfo=tzutc()),
 'expiry_timestamp': datetime.datetime(2021, 12, 31, 21, 26, 57, 647333, tzinfo=tzutc()),
 'name': 'nginx-200-iewliwlr',
 'namespace': 'system',
 'type': 'KUBE_CONFIG',
 'uid': '536abc7d-f033-4669-a745-6b09108d5eca',
 ...... truncated ......
 ```

List all expired Credentials.
```
python3 api-creds/api-creds.py --config /configs/config.json --listExpired

Number of Expired Credentials: 147
[{'active': True,
 'create_timestamp': datetime.datetime(2022, 2, 20, 18, 42, 26, 433296, tzinfo=tzutc()),
 'expiry_timestamp': datetime.datetime(2022, 3, 2, 18, 42, 26, 433302, tzinfo=tzutc()),
 'name': '1ab01fe-f3b4-3911-25ac-83f9bfe-omoshkgg',
 'namespace': 'system',
 'type': 'KUBE_CONFIG',
 'uid': '981b4030-6cf0-4ba6-847b-3c26c6da56a8',
...... truncated ......
```

Delete all Credentials listed in the provided file. 
You must confirm with 'y' or 'yes' to delete the Credentials.
```
python3 api-creds/api-creds.py --config /configs/config.json --deleteFromFile /configs/deleteAPICreds.txt

nginx-200-iewliwlr
nginx-200-shxmmihe
Delete Credentials Confirm: [y/n]n
Aborting Delete From File
```

If the delete is confirmed, the output will look like the following. 
```
python3 api-creds/api-creds.py --config /configs/config.json --deleteFromFile /configs/deleteAPICreds.txt

nginx-200-iewliwlr
nginx-200-shxmmihe
Delete Credentials Confirm: [y/n]y
Deleting Credential: {'active': True,
 'create_timestamp': datetime.datetime(2021, 11, 11, 21, 26, 57, 647325, tzinfo=tzutc()),
 'expiry_timestamp': datetime.datetime(2021, 12, 31, 21, 26, 57, 647333, tzinfo=tzutc()),
 'name': 'nginx-200-iewliwlr',
 'namespace': 'system',
 'type': 'KUBE_CONFIG',
 'uid': '536abc7d-f033-4669-a745-6b09108d5eca',
Deleting Credential: {'active': True,
 'create_timestamp': datetime.datetime(2021, 11, 11, 21, 26, 25, 594860, tzinfo=tzutc()),
 'expiry_timestamp': datetime.datetime(2021, 12, 31, 21, 26, 25, 594874, tzinfo=tzutc()),
 'name': 'nginx-200-shxmmihe',
 'namespace': 'system',
 'type': 'KUBE_CONFIG',
 'uid': 'cf912978-d9f3-4ffc-aab7-cc93f2188659',
 ```

Delete all expired Credentials.
You must confirm with 'y' or 'yes' to delete the Credentials.
```
python3 api-creds/api-creds.py --config /configs/config.json --deleteAllExpired

nginx-200-utrsvqua
nginx-200-wifvmrjm
nginx200-cfgtlxmx
nginx200-hdlsbuag
sentence-app-bckindhd
test-jj-creds-hyubmoeh
v8-gvnbwcwd
vjoshi1-dyabmcsp
Delete Credentials Confirm: [y/n]n
Aborting Delete All Expired
```


## alerts/get-alerts.py
The get-alerts helper fetches all alerts from the F5XC API. 

View the current alert types available.

```
python3 alerts/get-alerts.py --config /configs/config.json --listAlertTypes

KubePodCrashLooping
SiteSSHFailedLogin
SiteCustomerTunnelInterfaceDown
KubeDeploymentReplicasMismatch
ServiceEndpointHealthcheckFailure
KubePodNotReady
BotDefenseTooManySecurityEvents
KubeJobFailed
NodeLoadHigh
ServiceClientErrorPerSourceSite
ServiceServerErrorPerSourceSite
ServicePolicyTooManyAttacks
```

List all Alerts.

```
python3 alerts/get-alerts.py --config /configs/config.json --listAllAlerts

{'BotDefenseTooManySecurityEvents': [{'alertname': 'BotDefenseTooManySecurityEvents',
                                      'cluster_name': 'ny8-nyc-int-volterra-us',
                                      'group': 'Security',
                                      'identifier': 'acmecorp-tnxbsial/webapp-api-protection',
                                      'namespace': 'webapp-api-protection',
                                      'service_name': 'envoy',
                                      'severity': 'major',
                                      'site': 'ny8-nyc',
                                      'src_site': 'ny8-nyc',
                                      'tenant': 'acmecorp-tnxbsial',
                                      'vh_name': 'ves-io-http-loadbalancer-online-boutique-lb-2',
                                      'vh_type': 'HTTP-LOAD-BALANCER'}],
 'KubeDeploymentReplicasMismatch': [{'alertname': 'KubeDeploymentReplicasMismatch',
                                     'app': 'kube-state-metrics',
                                     'cluster_name': 'pa4-par-int-volterra-us',
                                     'deployment': 'cartservice',
                                     'group': 'IaaS-CaaS',
                                     'identifier': 'acmecorp-tnxbsial--api-security/cartservice',
                                     'job': 'kube-state-metrics',
                                     'k8s_ns': 'acmecorp-tnxbsial--api-security',
                                     'namespace': 'acmecorp-tnxbsial--api-security',
                                     'service_name': 'custom',
                                     'severity': 'minor',
                                     'site': 'pa4-par',
                                     'tenant': 'acmecorp-tnxbsial'},
....... Truncated .........
```

List all Alerts with the provided alert name.

```
python3 alerts/get-alerts.py --config /configs/config.json --listNamedAlerts BotDefenseTooManySecurityEvents

[{'alertname': 'BotDefenseTooManySecurityEvents', 'cluster_name': 'ny8-nyc-int-volterra-us', 'group': 'Security', 'namespace': 'webapp-api-protection', 'service_name': 'envoy', 'severity': 'major', 'site': 'ny8-nyc', 'src_site': 'ny8-nyc', 'tenant': 'acmecorp-tnxbsial', 'vh_name': 'ves-io-http-loadbalancer-online-boutique-lb-2', 'vh_type': 'HTTP-LOAD-BALANCER'}]
```


## policy/block-by-ip.py
The block-by-ip helper can add or remove an IP from a malicious user service policy. 

For this helper to work, any F5XC Load Balancer must be configured with a service policy. 
The service policy must have a deny list with an IP prefix set configured. 

Alternatively, the script will create a new service policy and add a new IP prefix set. 
This newly created service policy can then be added to the desired Load Balancers. 

### Required Arguments
--config <path to config file>

--namespace <namespace to use>

### Optional Arguments
--ipPrefixSet    
The IP prefix set to create/use for storing the blocked IP addresses.

Defaults to 'block-malicious-users' if not provided.


--policyName     
The Policy name to create, and link to the IP Prefix Set.

Defaults to 'block-malicious-users' if not provided.


To view all IP's currently in the blocking policy, use this command.

```
python3 policy/block-by-ip.py --config /configs/config.json --namespace ryan --viewBlockIP

['1.1.1.6/32', '10.10.10.1/32']
```

Add an IP to the block policy.

```
python3 policy/block-by-ip.py --config /configs/config.json --namespace ryan --blockIP 10.10.10.1
```

Remove an IP from the block policy.

```
python3 policy/block-by-ip.py --config /configs/config.json --namespace ryan --removeIP 10.10.10.1
```



