import f5xc_namespace_client
import f5xc_views_http_loadbalancer_client
import argparse
import json
import sys

sys.path.append("..")

import helpers.common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)


# uses client to call LB list API for all report fields, returns items attribue of response object
def getLB(client, namespace):
    LB = client.ves_io_schema_views_http_loadbalancer_api_list(namespace, report_fields='all')
    return LB.items

# check if we have cert info to report on, and then structure it
def formatCert(certInfo):
    myOut = {}
    if certInfo.auto_cert_subject is None:
        return None
    if certInfo.auto_cert_expiry is None:
        return None

    domain = certInfo.auto_cert_subject
    parts = domain.split("=")
    if len(parts) < 2:
        print("ERROR: not valid domain")
        return None

    myOut['domain'] = parts[-1]
    myOut['expires'] = certInfo.auto_cert_expiry.strftime("%m/%d/%Y, %H:%M:%S")

    return myOut


parser = argparse.ArgumentParser(description="Cert Expire Helper")
parser.add_argument('--config', default='config.json', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
args = parser.parse_args()

myConfig = config.Config(f5xc_namespace_client)
myConfig.readConfig(args.config)

nsClient = f5xc_namespace_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
api_instance = f5xc_namespace_client.DefaultApi(nsClient)
allNS = api_instance.ves_io_schema_namespace_api_list()

allNameSpace = []

for item in allNS.items:
    allNameSpace.append(item.name)

# we can reuse the config object as API's define the same config object fields
lbConfig = f5xc_views_http_loadbalancer_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))

lbClient = f5xc_views_http_loadbalancer_client.DefaultApi(lbConfig)

myOut = {}

for ns in allNameSpace:
    myOut[ns]={}

    allLBasList = getLB(lbClient, ns)

    for item2 in allLBasList:
        mySpec = item2.get_spec
        myName = item2.name
        if mySpec is None:
            next

        certObject = mySpec.auto_cert_info
        clean = formatCert(certObject)
        if clean is not None:
            myOut[ns][myName] = clean 
        
    if len(myOut[ns]) == 0:
        del(myOut[ns])

print(json.dumps(myOut, indent =4))
