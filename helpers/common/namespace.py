import f5xc_namespace_client

sys.path.append('../')

import common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)

def getNSClient(configFile):
    myConfig = config.Config(f5xc_namespace_client)
    myConfig.readConfig(configFile)

    nsClient = f5xc_namespace_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
    apiInstance = f5xc_namespace_client.DefaultApi(nsClient)

    return apiInstance

# getALLNS returns a list of all namespaces present on the console 
def getAllNS(apiInstance):
    allNS = apiInstance.ves_io_schema_namespace_api_list()
    allNameSpace = []

    for item in allNS.items:
        allNameSpace.append(item.name)

    return allNameSpace

