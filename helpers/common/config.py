import json
import sys

class Config:
    def __init__(self, sdkClient):
        self.clientConfig = None
        self.apiToken = None
        self.sdkClient = sdkClient
    
    def readConfig(self, myFile):
        data = {}
        try:
            with open(myFile) as f:
                data = json.load(f)
                f.close()
        except Exception as e:
            print("Error opening file: {} with error: {}".format(myFile, e))
            sys.exit(2)

        config = self.sdkClient.Configuration()

        for k, v in data.items():
            if k == 'ApiKey':
                config.api_key = v
            elif k == 'APIToken':
                self.apiToken = v
            elif k == 'Debug':
                config.debug = v
            elif k == 'Host':
                config.host = v
            elif k == 'VerifySSL':
                config.verify_ssl = v
            else:
                print("Provided Config Option {} is not currently supported".format(k))
                
        self.clientConfig = config