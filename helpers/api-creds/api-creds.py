import f5xc_api_credential_client
import argparse
import sys
import json
import pprint
import datetime

sys.path.append('..')

import helpers.common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)

def getAllCredsByType(apiInstance):
    creds = {}
    allCreds = apiInstance.ves_io_schema_api_credential_custom_api_list('system')
    for item in allCreds.items:
        if item.type not in creds:
            creds[item.type] = []
        creds[item.type].append(item)
    return creds

def getAllCreds(apiInstance):
    creds = {}
    allCreds = apiInstance.ves_io_schema_api_credential_custom_api_list('system')
    for item in allCreds.items:
        creds[item.name]=item
    return creds

def getExpired(allCreds):
    expired = []
    now = datetime.datetime.now().astimezone()
    for _, cred in allCreds.items():
        if cred.expiry_timestamp < now:
            expired.append(cred)
    return expired

def deleteCred(apiInstance, cred):
    try:
        print("Deleting Credential: {}".format(cred))
        myCredJson ={ 
            "namespace": cred.namespace, 
            "name": cred.name, 
            "spec": {"type" : cred.type}
        } 
        apiInstance.ves_io_schema_api_credential_custom_api_revoke("system", myCredJson)
    except Exception as e:
        print("ERROR deleting cred {} with error {}".format(cred, e))
        sys.exit(2)

def deleteFromFile(apiInstance, allCreds, myFile):
    data = ''
    try:
        f = open(myFile, 'r')
        data = f.read()
        f.close()
    except Exception as e:
        print("ERROR unable to read creds delete file: {} with error: {}".format(myFile, e))
        sys.exit(2)

    print(data)
    sys.stdout.write('Delete Credentials Confirm: [y/n]')
    resp = input().lower()
    if resp in ['yes', 'y']:
        for c in data.split('\n'):
            if len(c) > 4:
                if c in allCreds:
                    deleteCred(apiInstance, allCreds[c])
    else:
        print("Aborting Delete From File")


def deleteAllExpired(apiInstance, expiredCreds):
    for cred in expiredCreds:
        print(cred.name)

    sys.stdout.write('Delete Credentials Confirm: [y/n]')
    resp = input().lower()
    if resp in ['yes', 'y']:
        for cred in expiredCreds:
            deleteCred(apiInstance, cred)
    else:
        print("Aborting Delete All Expired")

parser = argparse.ArgumentParser(description="f5xc_api_credential_client Helper")
parser.add_argument('--config', default='config.json', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
parser.add_argument('--deleteAllExpired', action='store_true',help='remove any credential that is expired')
parser.add_argument('--listExpired', action='store_true',help='list any credential that is expired')
parser.add_argument('--listTypes', action='store_true', help='list all found credential Types')
parser.add_argument('--listAllType', help='list all credentials by given Type')
parser.add_argument('--listAllCreds', action='store_true', help='List all creds')
parser.add_argument('--deleteFromFile', help='remove any credential listed in provided file')

args = parser.parse_args()

myConfig = config.Config(f5xc_api_credential_client)
myConfig.readConfig(args.config)

try: 
    myClient = f5xc_api_credential_client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
    apiInstance = f5xc_api_credential_client.DefaultApi(myClient)
except Exception as e:
    print("ERROR creating instance with error: ", e)

if bool(args.listTypes):
    allCredsByType = getAllCredsByType(apiInstance)
    print(allCredsByType.keys())
    sys.exit(0)

if args.listAllType is not None:
    allCredsByType = getAllCredsByType(apiInstance)
    if args.listAllType not in allCredsByType.keys():
        print("ERROR: Given Credential Type is not found: ", args.listAllType)
        sys.exit(2)
    pprint.pprint(allCredsByType[args.listAllType])

allCreds = getAllCreds(apiInstance)

if bool(args.listAllCreds):
    pprint.pprint(allCreds)

if bool(args.listExpired):
    expired = getExpired(allCreds)
    print("Number of Expired Credentials: {}".format(len(expired)))
    pprint.pprint(expired)
    sys.exit(0)

if args.deleteFromFile is not None:
    deleteFromFile(apiInstance, allCreds, args.deleteFromFile)
    sys.exit(0)

if bool(args.deleteAllExpired):
    deleteAllExpired(apiInstance, getExpired(allCreds))