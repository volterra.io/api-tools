import $client
import argparse
import sys
import json
import pprint

sys.path.append('../')

import common.namespace as namespace
import common.config as config

from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings
disable_warnings(InsecureRequestWarning)


parser = argparse.ArgumentParser(description="$client Helper")
parser.add_argument('--config', default='config.json', help='json config file containing "APIToken" and "Host" entries. Defaults to "config.json"')
args = parser.parse_args()

myConfig = config.Config($client)
myConfig.readConfig(args.config)

myClient = $client.ApiClient(myConfig.clientConfig, 'Authorization', 'APIToken {}'.format(myConfig.apiToken))
apiInstance = $client.DefaultApi(myClient)
pprint.pprint(dir(apiInstance))


getNSClient = namespace.getAllNS(args.config)
print(namespace.getAllNS(getNSClient))